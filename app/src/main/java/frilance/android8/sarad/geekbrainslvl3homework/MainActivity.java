package frilance.android8.sarad.geekbrainslvl3homework;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.functions.Func1;


public class MainActivity extends AppCompatActivity {


    private TextView textView;
    private EditText editText;


    private Subscription subscription;
    private Observer<String> observer;
    private Func1<String, String> function;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.text_next);
        editText = findViewById(R.id.edit_first);

        observer = getObserver();
        function = getFunction();

        editText.addTextChangedListener(getWatcher());
    }


    @NonNull
    private Func1<String, String> getFunction() {
        return new Func1<String, String>() {

            @Override
            public String call(String s) {
                return s;
            }
        };
    }


    @NonNull
    private Observer<String> getObserver() {
        return new Observer<String>() {
            @Override
            public void onCompleted() {
                Log.d("Dto", "onCompleted");
            }

            @Override
            public void onError(Throwable e) {
                Log.d("Dto", "onError");
                e.printStackTrace();
            }

            @Override
            public void onNext(String s) {
                textView.setText(s);
            }
        };
    }


    @NonNull
    private TextWatcher getWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                subscription = Observable.just(editText.getText().toString())
                        .distinct()
                        .map(function)
                        .subscribe(observer);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };


    }


}
